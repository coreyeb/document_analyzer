from __future__ import unicode_literals

from django.apps import apps


def analyze_document(sender, instance, **kwargs):
    """Analyze document version"""

    Result = apps.get_model(
        app_label='document_analyzer', model_name='Result'
    )
    Result.objects.process_version(document_version=instance)
