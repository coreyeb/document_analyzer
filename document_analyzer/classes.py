from __future__ import unicode_literals

from django.core.exceptions import ObjectDoesNotExist
from django.utils.translation import ugettext_lazy as _


class DocumentVersionAnalyzeResultHelper(object):
    @staticmethod
    @property
    def constructor(source_object):
        return DocumentVersionAnalyzeResultHelper(source_object)

    def __init__(self, instance):
        self.instance = instance

    def __getattr__(self, parameter):
        try:
            return self.instance.analyzerresult.get(parameter=parameter).value
        except ObjectDoesNotExist:
            raise AttributeError(
                _('Result parameter \'%s\' not found') % parameter
            )


class DocumentAnalyzeResultHelper(DocumentVersionAnalyzeResultHelper):
    @staticmethod
    @property
    def constructor(source_object):
        return DocumentAnalyzeResultHelper(source_object)

    def __init__(self, instance):
        self.instance = instance.latest_version
