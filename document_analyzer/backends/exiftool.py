from __future__ import unicode_literals

import json
import logging
import subprocess
import tempfile

from common.utils import fs_cleanup

from .literals import EXIFTOOL_PATH

logger = logging.getLogger(__name__)


class EXIFTool(object):

    def execute(self, document_version, parameter=None):
        _new_file_object, temp_filename = tempfile.mkstemp()

        try:
            document_version.save_to_file(filepath=temp_filename)
            result = subprocess.check_output([EXIFTOOL_PATH, '-j', temp_filename])
            return json.loads(result)[0].items()
        finally:
            fs_cleanup(filename=temp_filename)
