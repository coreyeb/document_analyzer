from __future__ import absolute_import, unicode_literals

from django.contrib import messages
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import get_object_or_404
from django.template import RequestContext
from django.utils.translation import ugettext_lazy as _

from acls.models import AccessControlList
from common.generics import (
    ConfirmView, AssignRemoveView, SingleObjectCreateView, SingleObjectDeleteView,
    SingleObjectEditView, SingleObjectListView
)
from documents.models import Document, DocumentVersion
from documents.permissions import permission_document_view
from permissions import Permission

from .icons import icon_analyzer
from .links import link_analyzer_create
from .models import Analyzer
from .permissions import (
    permission_analyzer_create, permission_analyzer_delete,
    permission_analyzer_edit, permission_analyzer_view,
    permission_analyze_document,
)


class DocumentSubmitView(ConfirmView):
    def get_extra_context(self):
        return {
            'object': self.get_object(),
            'title': _('Submit "%s" to the Analyzer queue?') % self.get_object()
        }

    def get_object(self):
        return Document.objects.get(pk=self.kwargs['pk'])

    def object_action(self, instance):
        try:
            Permission.check_permissions(
                self.request.user, (permission_analyze_document,)
            )
        except PermissionDenied:
            AccessControlList.objects.check_access(
                permission_analyze_document, self.request.user, instance
            )

        instance.submit_for_analyze()

    def view_action(self):
        instance = self.get_object()

        self.object_action(instance=instance)

        messages.success(
            self.request,
            _('Document: %(document)s was added to the Analyzer queue.') % {
                'document': instance
            }
        )


class DocumentAllSubmitView(ConfirmView):
    extra_context = {'title': _('Submit all documents for analyzing?')}

    def get_post_action_redirect(self):
        return reverse('common:tools_list')

    def view_action(self):
        count = 0
        for document in Document.objects.filter(is_stub=False):
            document.submit_for_analyze()
            count += 1

        messages.success(
            self.request, _('%d documents added to the analyzer queue.') % count
        )


class AnalyzerListView(SingleObjectListView):
    model = Analyzer
    view_permission = permission_analyzer_view

    def get_extra_context(self):
        return {
            'hide_object': True,
            'title': _('Analyzers'),
        }


class AnalyzerCreateView(SingleObjectCreateView):
    fields = (
        'label', 'document_types', 'slug', 'parameter', 'type'
    )
    model = Analyzer
    post_action_redirect = reverse_lazy('document_analyzer:analyzer_list')
    view_permission = permission_analyzer_create

    def get_extra_context(self):
        return {
            'title': _('Create analyzer'),
        }


class AnalyzerDeleteView(SingleObjectDeleteView):
    model = Analyzer
    post_action_redirect = reverse_lazy('document_analyzer:analyzer_list')
    view_permission = permission_analyzer_delete

    def get_extra_context(self):
        return {
            'object': self.get_object(),
            'title': _('Delete the analyzer: %s?') % self.get_object(),
        }


class AnalyzerEditView(SingleObjectEditView):
    fields = (
        'label', 'document_types', 'slug', 'type', 'parameter'
    )
    model = Analyzer
    post_action_redirect = reverse_lazy('document_analyzer:analyzer_list')
    view_permission = permission_analyzer_edit

    def get_extra_context(self):
        return {
            'object': self.get_object(),
            'title': _('Edit analyzer: %s') % self.get_object(),
        }


class DocumentVersionResultView(SingleObjectListView):
    def get_extra_context(self):
        return {
            'hide_object': True,
            'object': self.get_object(),
            'document': self.get_object().document,
            'navigation_object_list': ('object', 'document'),
            'title': _('Analyzer result for: %s') % self.get_object(),
        }

    def get_object(self):
        document_version = get_object_or_404(
            DocumentVersion, pk=self.kwargs['pk']
        )

        try:
            Permission.check_permissions(
                self.request.user, (permission_document_view,)
            )
        except PermissionDenied:
            AccessControlList.objects.check_access(
                permission_document_view, self.request.user, document_version
            )

        return document_version

    def get_object_list(self):
        return self.get_object().analyzerresult.all()


class DocumentResultView(SingleObjectListView):
    def dispatch(self, request, *args, **kwargs):
        AccessControlList.objects.check_access(
            permissions=permission_document_view,
            user=self.request.user, obj=self.get_document()
        )

        return super(DocumentResultView, self).dispatch(
            request, *args, **kwargs
        )

    def get_document(self):
        return get_object_or_404(Document, pk=self.kwargs['pk'])

    def get_extra_context(self):
        document = self.get_document()
        return {
            'hide_object': True,
            'object': document,
            'no_results_icon': icon_analyzer,
            'no_results_main_link': link_analyzer_create.resolve(
                context=RequestContext(
                    request=self.request, dict_={'object': document}
                )
            ),
            'no_results_text': _(
                'Add and analyzer to this document\'s type '
                'and start . '
                'Once added to individual document, you can then edit their '
                'values.'
            ),
            'no_results_title': _('This document doesn\'t have any analyzer result'),
            'title': _('Analyzer result for: %s') % document,
        }

        try:
            Permission.check_permissions(
                self.request.user, (permission_document_view,)
            )
        except PermissionDenied:
            AccessControlList.objects.check_access(
                permission_document_view, self.request.user, document
            )

        return document

    def get_object_list(self):
        return self.get_document().latest_version.analyzerresult.all()


class SetupAnalyzerDocumentTypesView(AssignRemoveView):
    decode_content_type = True
    object_permission = permission_analyzer_edit
    left_list_title = _('Available document types')
    right_list_title = _('Document types assigned this analyzer')

    def add(self, item):
        self.get_object().document_types.add(item)

    def get_extra_context(self):
        return {
            'title': _(
                'Document types assigned the analyzer: %s'
            ) % self.get_object(),
            'object': self.get_object(),
        }

    def get_object(self):
        return get_object_or_404(Analyzer, pk=self.kwargs['pk'])

    def left_list(self):
        return AssignRemoveView.generate_choices(
            self.get_object().get_document_types_not_in_analyzer()
        )

    def right_list(self):
        return AssignRemoveView.generate_choices(
            self.get_object().document_types.all()
        )

    def remove(self, item):
        self.get_object().document_types.remove(item)
